import {Component} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  fileName = '';

  constructor(private http: HttpClient) {
  }

  onFileSelected(event: any) {

    const file: File = event.target.files[0];

    if (file) {

      this.fileName = file.name;

      const formData = new FormData();

      formData.append("file", file, file.name);
      formData.append("connectAtStartup", "true");
      let headers = new HttpHeaders();
      // headers = headers.append('Content-Type', 'multipart/form-data');
      headers = headers.append('Accept', '*/*');

     this.http.post("http://localhost:5005/api/configuration/vpn", formData, {
       headers
      }).subscribe({
        next: (data: any) => console.log(data),
        error: (error: any) => console.log(error)
      });
    }
  }


}
